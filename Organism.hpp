#ifndef ORGANISM_H
#define ORGANISM_H

#include <string>
#include <string.h>

#include "objectiveFunction/ObjectiveFunction.hpp"
#include "fitnessFunction/FitnessFunction.hpp"

class Organism{
public:
	Organism()
	{
		m_chromosome = 0;
		m_size = 0;
	};
	Organism(int size);
	~Organism();
	double getObjectiveFunction();
	double getFitness();
	std::size_t size();
	void evaluate(ObjectiveFunction * objFunction, FitnessFunction * fitFunction);
	Organism & operator = (const Organism &);
	double & operator[](unsigned int index);
private:
	double * m_chromosome;
	double m_objectiveFunction;
	double m_fitness;
	std::size_t m_size;	
};

Organism::Organism(int size)
{
	m_size = size;
	m_chromosome = new double[m_size];
}

//TODO: 
Organism::~Organism()
{
	if(m_chromosome != 0)
	{
		delete[] m_chromosome;
		m_chromosome = 0;
	}	
}

double Organism::getObjectiveFunction()
{
	return m_objectiveFunction;
}

double Organism::getFitness()
{
	return m_fitness;
}

std::size_t Organism::size()
{
	return m_size;
}


void Organism::evaluate(ObjectiveFunction * objFunction, FitnessFunction * fitFunction)
{
	m_objectiveFunction = objFunction->evaluate(m_chromosome,m_size);
	m_fitness = fitFunction->evaluate(m_objectiveFunction);
}

Organism& Organism::operator = (const Organism & other)
{
	if (this == &other) {
		return *this;
	}
	if(m_size != other.m_size && m_chromosome != 0)
	{
		delete [] m_chromosome;
		m_chromosome = 0;
	}
	m_size = other.m_size;
	if(m_chromosome == 0)
	{
		m_chromosome = new double[m_size];
	}
	memcpy ( m_chromosome, other.m_chromosome, m_size * sizeof(double) );

	m_objectiveFunction = other.m_objectiveFunction;
	m_fitness = other.m_fitness;
	return *this;
}

double& Organism::operator[](unsigned int index) {
    return m_chromosome[index];
}

#endif
