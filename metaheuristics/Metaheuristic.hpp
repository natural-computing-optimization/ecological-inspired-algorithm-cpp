#ifndef METAHEURISTIC_H
#define METAHEURISTIC_H

#include<string>

using std::string;

class Metaheuristic{
public:
	virtual void execute(Specie* specie) = 0;
	virtual string getName() = 0;
};


#endif
