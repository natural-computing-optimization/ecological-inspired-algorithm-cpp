#ifndef SOS_P_H
#define SOS_P_H

#include <string>

#include "../Metaheuristic.hpp"

class SOS_P : public Metaheuristic{
public:
	SOS_P(Configuration config)
	{
		m_config = config;
	};
	~SOS_P()
	{
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organismA;
	Organism m_organismB;
};

void SOS_P::execute(Specie* specie)
{
	unsigned short int index = 0;	
	unsigned short int other = 0;
	unsigned short int j = 0;
	unsigned short int currentBestIndex = 0;
	double mutual_vector = 0;
	int BF1 = 0;
	int BF2 = 0;
	Organism m_best;
	
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	while(specie->hasEvolutionaryPeriod())
	{
		
		for(index = 0; index < specie->size(); index++)
		{
						
			//===============================
			// Parasitism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			m_organismA = specie->operator[](index);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			m_organismA[j] = m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(j),m_config.getObjectiveFunction()->getUpperBound(j));
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](other).getObjectiveFunction())
			{
				specie->operator[](other) = m_organismA;
			}		
			
			//===============================
			// Find current best
			//===============================
			specie->findCurrentBest();
			
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}
		}
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string SOS_P::getName()
{
	return "Symbiotic Organism Search - P";
}

#endif
