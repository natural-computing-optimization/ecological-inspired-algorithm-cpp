#ifndef ARTIFICIAL_BEE_COLONY_OPTIMIZATION_H
#define ARTIFICIAL_BEE_COLONY_OPTIMIZATION_H

#include <string>

#include "../Metaheuristic.hpp"

class ArtificialBeeColonyOptimization : public Metaheuristic{
public:
	ArtificialBeeColonyOptimization(Configuration config)
	{
		m_limit = 100;
		m_config = config;
		probabilities = new double[m_config.getSpecieSize()];
		trial = new unsigned short int[m_config.getSpecieSize()];
		for(unsigned short int a = 0; a < m_config.getSpecieSize(); a++)
		{
			trial[a] = 0;
		}
	};
	~ArtificialBeeColonyOptimization()
	{
			delete []trial;
			delete []probabilities;
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	int m_limit;
	Configuration m_config;
	Aleatory m_aleatory;
	double m_maxFitness;
	Organism m_organism;
	unsigned short int * trial;
	double * probabilities;
};

void ArtificialBeeColonyOptimization::execute(Specie* specie)
{
	unsigned short int a = 0;
	unsigned short int index = 0;
	unsigned short int param2change = 0;
	unsigned short int neighbour = 0;


	specie->initEvolutionaryPeriod();

	while(specie->hasEvolutionaryPeriod())
	{
		//===============================
		// Employ Bee Phase
		//===============================
		for(index = 0; index < specie->size(); index++)
		{
			//----------------------------------------------
			// The parameter to be changed is determined randomly
			//----------------------------------------------
			param2change = m_aleatory.nextInt(m_config.getDimensions());
			//----------------------------------------------
			// A randomly chosen solution is used in producing a mutant solution of the solution index
			// Randomly selected solution must be different from the solution index*/    
			//----------------------------------------------    
			do
			{
				neighbour = m_aleatory.nextInt(specie->size());
			}while(neighbour == index );
			m_organism = specie->operator[](index);
			//----------------------------------------------
			// v_{ij}=x_{ij}+\phi_{ij}*(x_{kj}-x_{ij}) */
			//----------------------------------------------
			m_organism[param2change] = m_organism[param2change] + (m_organism[param2change] - specie->operator[](neighbour)[param2change]) * (m_aleatory.nextDouble() - 0.5) * 2.0;
			//==============================
			// set limits
			//==============================	
			m_organism[param2change] = m_config.getObjectiveFunction()->verifyBounds(param2change,m_organism[param2change]);	
			//==============================
			// Evaluate
			//==============================
			m_organism.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			if(m_organism.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
			{
				specie->operator[](index) = m_organism;
				trial[index] = 0;
			}
			else
			{
				trial[index]++;
			}
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}
		}
		
		
		//===============================
		// Find best organism
		//===============================
		specie->findCurrentBest();
		
		//===============================
		// Calcule probabilities
		//===============================
		m_maxFitness = specie->getCurrentBest().getFitness();
		
		
		for(a = 0; a < specie->size(); a++)
		{
			probabilities[a] = ( 0.9 * (specie->operator[](a).getFitness() / m_maxFitness ) ) + 0.1;
		}
		
		
		//===============================
		// Onlooker Bee Phase
		//===============================
		index = 0;
		a = 0;
		while(a < specie->size() && specie->hasEvolutionaryPeriod())
		{
			if(m_aleatory.nextDouble() < probabilities[index])
			{
				a++;
				//----------------------------------------------
				// The parameter to be changed is determined randomly
				//----------------------------------------------
				param2change = m_aleatory.nextInt(m_config.getDimensions());
				//----------------------------------------------
				// A randomly chosen solution is used in producing a mutant solution of the solution index
				// Randomly selected solution must be different from the solution index*/    
				//----------------------------------------------    
				do
				{
					neighbour = m_aleatory.nextInt(specie->size());
				}while(neighbour == index );
				m_organism = specie->operator[](index);
				//----------------------------------------------
				// v_{ij}=x_{ij}+\phi_{ij}*(x_{kj}-x_{ij}) */
				//----------------------------------------------
				m_organism[param2change] = m_organism[param2change] + (m_organism[param2change] - specie->operator[](neighbour)[param2change]) * (m_aleatory.nextDouble() - 0.5) * 2.0;
				//==============================
				// set limits
				//==============================	
				m_organism[param2change] = m_config.getObjectiveFunction()->verifyBounds(param2change,m_organism[param2change]);	
				//==============================
				// Evaluate
				//==============================
				m_organism.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());				
				specie->incrementEvaluations();
				
				if(m_organism.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
				{
					specie->operator[](index) = m_organism;
					trial[index] = 0;
				}
				else
				{
					trial[index]++;
				}
			}
			index++;
			if(index == (specie->size() -1) )
			{
				index = 0;
			}
		}
		
		
		specie->findCurrentBest();
		//===============================
		// Scout Bee Phase
		//===============================
		index = 0;
		for(a = 1; a < specie->size(); a++)
		{
			if(trial[a] > trial[index])
			{
				index = a;
			}
		}
		if(trial[index] >= m_limit && specie->hasEvolutionaryPeriod()) 
		{
			for(a = 0; a < m_organism.size(); a++)
			{
				m_organism[a] = m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(a),m_config.getObjectiveFunction()->getUpperBound(a));
			}
			//==============================
			// Evaluate
			//==============================
			m_organism.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			//==============================
			// Update
			//==============================
			specie->operator[](index) = m_organism;
			trial[index] = 0;			
		}
	}
	
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
	
	
}

string ArtificialBeeColonyOptimization::getName()
{
	return "Artificial Bee Colony Optimization";
}

#endif
