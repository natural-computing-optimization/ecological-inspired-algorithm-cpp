#ifndef BAT_ALGORITHM_H
#define BAT_ALGORITHM_H

#include <math.h>
#include <string>

#include "../Metaheuristic.hpp"

/*
 * CORDEIRO, J. ; PARPINELLI, R. S. ; LOPES, H. S. . 
 * Análise de Sensibilidade dos Parâmetros do Bat Algorithm e Comparação de Desempenho. 
 * In: Encontro Nacional de Inteligência Artificial (ENIA), 2012, Curitiba. Brazilian Conference on Intelligent System (BRACIS’2012), 2012. v. 1. p. 1-9.
 */
class BatAlgorithm : public Metaheuristic{
public:
	BatAlgorithm(Configuration config)
	{
		unsigned short int d = 0;
		m_config = config;
		m_iteration = 0;
		m_alpha = 0.5;
		m_lambda = 0.1;
		m_velocity = new Organism[m_config.getSpecieSize()];		
		m_pulseRates = new double[m_config.getSpecieSize()];
		m_loudness = new double[m_config.getSpecieSize()];
		
		for(unsigned short int a =0; a < m_config.getSpecieSize(); a++)
		{
				m_velocity[a] = Organism(m_config.getDimensions());
				for(d = 0; d < m_config.getDimensions(); d++)
				{
					m_velocity[a][d] = 0;//m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(d),m_config.getObjectiveFunction()->getUpperBound(d));
				}
				m_pulseRates[a] = 0;
				m_loudness[a] = 1;
		}
	};
	~BatAlgorithm()
	{
		delete [] m_velocity;
		delete [] m_pulseRates;
		delete [] m_loudness;
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organism;
	Organism * m_velocity;
	double m_f_min;
	double m_f_max;
	double * m_pulseRates;
	double * m_loudness;
	double m_alpha;
	double m_lambda;
	int m_iteration;
};

void BatAlgorithm::execute(Specie* specie)
{
	double frequency = 0;
	double averageLoudness = 0;
	unsigned short int d;
	unsigned short int i;
	unsigned short int currentBest;
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	currentBest = specie->getCurrentBestIndex();

	while(specie->hasEvolutionaryPeriod())
	{
		//========================
		// For each organism (candidate solution)
		//========================
		for(i =0; i < m_config.getSpecieSize(); i++)
		{
			
			if(!specie->hasEvolutionaryPeriod())
			{
					break;
			}
			
			//=========================
			// Get a copy to modify
			//=========================
			m_organism = specie->operator[](i);
			
				
			//=========================
			// Update frequency
			//=========================
			for(d =0; d < m_config.getDimensions(); d++)
			{
				m_f_min = m_config.getObjectiveFunction()->getLowerBound(d);
				m_f_max = m_config.getObjectiveFunction()->getUpperBound(d);
				frequency = m_f_min + (m_f_max - m_f_min) * m_aleatory.nextDouble();
				m_velocity[i][d] = m_velocity[i][d] + (m_organism[d] - specie->operator[](currentBest)[d])*frequency;
				m_organism[d] += m_velocity[i][d];
				//==============================
				// set limits
				//==============================	
				m_organism[d] = m_config.getObjectiveFunction()->verifyBounds(d,m_organism[d]);	
				
			}
			
			//====================
			// local search
			//====================
			if(m_aleatory.nextDouble() < m_pulseRates[i])
			{				
				averageLoudness = 0;
				//====================
				// calcule average loudness
				//====================
				for(d =0; d < m_config.getSpecieSize(); d++)
				{
					averageLoudness += m_loudness[d];
				}
				averageLoudness /= m_config.getSpecieSize();
				
				for(d =0; d < m_config.getDimensions(); d++)
				{
					m_organism[d] = specie->operator[](currentBest)[d] + m_aleatory.nextDouble(-1,1) * averageLoudness;					
					//==============================
					// set limits
					//==============================	
					m_organism[d] = m_config.getObjectiveFunction()->verifyBounds(d,m_organism[d]);	
				}
			}			 			
			
			//=========================
			// flying randomly
			//=========================
			d = m_aleatory.nextInt(m_config.getDimensions());
			m_organism[d] = m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(d),m_config.getObjectiveFunction()->getUpperBound(d));
			
			//=========================
			// Evaluate new organism
			//=========================
			m_organism.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());			
			specie->incrementEvaluations();
			
			//=========================
			// Replace if better
			//=========================	
			if(m_aleatory.nextDouble() < m_loudness[i] || m_organism.getObjectiveFunction() < specie->operator[](i).getObjectiveFunction())
			{
				specie->operator[](i) = m_organism; 
				m_loudness[i] = m_alpha * m_loudness[i];
				m_pulseRates[i] =  ( 1.0 - exp(-m_lambda * m_iteration));
			}
			
			//=========================
			// Find current best solution
			//=========================	
			specie->findCurrentBest();
			currentBest = specie->getCurrentBestIndex();
			
		}
		m_iteration++;
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string BatAlgorithm::getName()
{
	return "Bat Algorithm";
}

#endif
