#ifndef SOS_C_H
#define SOS_C_H

#include <string>

#include "../Metaheuristic.hpp"

class SOS_C : public Metaheuristic{
public:
	SOS_C(Configuration config)
	{
		m_config = config;
	};
	~SOS_C()
	{
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organismA;
	Organism m_organismB;
};

void SOS_C::execute(Specie* specie)
{
	unsigned short int index = 0;	
	unsigned short int other = 0;
	unsigned short int j = 0;
	unsigned short int currentBestIndex = 0;
	double mutual_vector = 0;
	int BF1 = 0;
	int BF2 = 0;
	Organism m_best;
	
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	while(specie->hasEvolutionaryPeriod())
	{
		
		for(index = 0; index < specie->size(); index++)
		{
			currentBestIndex = specie->getCurrentBestIndex();
			m_best = specie->operator[](currentBestIndex);
						
			//===============================
			// Commensalism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			m_organismA = specie->operator[](index);
			m_organismB = specie->operator[](other);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			//for(j = 0; j < m_config.getDimensions(); j++)
			//{
					m_organismA[j] = m_organismA[j] + m_aleatory.nextDouble(-1,1) * (m_best[j] - m_organismB[j]);	
					//==============================
					// set limits
					//==============================	
					m_organismA[j] = m_config.getObjectiveFunction()->verifyBounds(j,m_organismA[j]);									
			//}
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
			{
				specie->operator[](index) = m_organismA;
			}
			
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}	
			
			//===============================
			// Find current best
			//===============================
			specie->findCurrentBest();
			
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}
		}
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string SOS_C::getName()
{
	return "Symbiotic Organism Search - ";
}

#endif
