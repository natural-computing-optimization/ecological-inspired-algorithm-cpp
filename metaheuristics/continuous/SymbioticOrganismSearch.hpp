#ifndef SYMBIOTIC_ORGANISM_SEARCH_H
#define SYMBIOTIC_ORGANISM_SEARCH_H

#include <string>

#include "../Metaheuristic.hpp"

class SymbioticOrganismSearch : public Metaheuristic{
public:
	SymbioticOrganismSearch(Configuration config)
	{
		m_config = config;
	};
	~SymbioticOrganismSearch()
	{
	};
	virtual void execute(Specie* specie);
	virtual string getName();
private:
	Configuration m_config;
	Aleatory m_aleatory;
	Organism m_organismA;
	Organism m_organismB;
};

void SymbioticOrganismSearch::execute(Specie* specie)
{
	unsigned short int index = 0;	
	unsigned short int other = 0;
	unsigned short int j = 0;
	unsigned short int currentBestIndex = 0;
	double mutual_vector = 0;
	int BF1 = 0;
	int BF2 = 0;
	Organism m_best;
	
	//===============================
	// Init Evolutionary Period
	//===============================
	specie->initEvolutionaryPeriod();

	while(specie->hasEvolutionaryPeriod())
	{
		
		for(index = 0; index < specie->size(); index++)
		{
			currentBestIndex = specie->getCurrentBestIndex();
			m_best = specie->operator[](currentBestIndex);
			//===============================
			// Mutualism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			
			
			m_organismA = specie->operator[](index);
			m_organismB = specie->operator[](other);
			
			BF1 = m_aleatory.nextInt(1,2);
			BF2 = m_aleatory.nextInt(1,2);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			
			//for(j = 0; j < m_config.getDimensions(); j++)
			//{
					mutual_vector = (m_organismA[j] - m_organismB[j]) / 2;
					m_organismA[j] = m_organismA[j] + m_aleatory.nextDouble() * (m_best[j] - mutual_vector * BF1);		
					m_organismB[j] = m_organismB[j] + m_aleatory.nextDouble() * (m_best[j] - mutual_vector * BF2);	
					//==============================
					// set limits
					//==============================	
					m_organismA[j] = m_config.getObjectiveFunction()->verifyBounds(j,m_organismA[j]);	
					m_organismB[j] = m_config.getObjectiveFunction()->verifyBounds(j,m_organismB[j]);								
			//}
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();			
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
			{
				specie->operator[](index) = m_organismA;
			}
			
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}
			
			m_organismB.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			
			
			if(m_organismB.getObjectiveFunction() < specie->operator[](other).getObjectiveFunction())
			{
				specie->operator[](other) = m_organismB;
			}
			
			//===============================
			// Commensalism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			m_organismA = specie->operator[](index);
			m_organismB = specie->operator[](other);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			
			//for(j = 0; j < m_config.getDimensions(); j++)
			//{
					m_organismA[j] = m_organismA[j] + m_aleatory.nextDouble(-1,1) * (m_best[j] - m_organismB[j]);	
					//==============================
					// set limits
					//==============================	
					m_organismA[j] = m_config.getObjectiveFunction()->verifyBounds(j,m_organismA[j]);									
			//}
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](index).getObjectiveFunction())
			{
				specie->operator[](index) = m_organismA;
			}
			
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}
			
			//===============================
			// Parasitism phase
			//===============================
			do
			{
				other = m_aleatory.nextInt(specie->size());
			}while(other == index);
			
			m_organismA = specie->operator[](index);
			
			j = m_aleatory.nextInt(m_config.getDimensions());
			m_organismA[j] = m_aleatory.nextDouble(m_config.getObjectiveFunction()->getLowerBound(j),m_config.getObjectiveFunction()->getUpperBound(j));
			
			m_organismA.evaluate(m_config.getObjectiveFunction(),m_config.getFitnessFunction());
			specie->incrementEvaluations();
			
			if(m_organismA.getObjectiveFunction() < specie->operator[](other).getObjectiveFunction())
			{
				specie->operator[](other) = m_organismA;
			}		
			
			//===============================
			// Find current best
			//===============================
			specie->findCurrentBest();
			
			if(!specie->hasEvolutionaryPeriod())
			{
				break;
			}
		}
	}
	
	//===============================
	// Finish Evolutionary Period
	//===============================
	specie->finishEvolutionaryPeriod();
}

string SymbioticOrganismSearch::getName()
{
	return "Symbiotic Organism Search";
}

#endif
