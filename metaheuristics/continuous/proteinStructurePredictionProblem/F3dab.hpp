#ifndef F3DAB_H
#define F3DAB_H

#include<math.h>
#include<string>

#include"Protein.h"
#include"../../Aleatory.hpp"


class F3dab : public ObjectiveFunction{
public:
	F3dab(std::string sequence)
	{
		m_lowerBound = -1*PI;
		m_upperBound =  PI;
		m_sequence = sequence;
		std::cout << " Sequence :: " << sequence << std::endl;
		m_proteinSize = m_sequence.size();
		m_aminoAcido = (amino *) malloc ((m_proteinSize) * sizeof (amino));
		for(unsigned int i=0;i < m_proteinSize;i++)
	   	{
			if (m_sequence[i] == 'A' || m_sequence[i] == 'a')
			{
				m_aminoAcido[i].Tipo = 1;
			}
			if (m_sequence[i] == 'B' || m_sequence[i] == 'b')
			{
				m_aminoAcido[i].Tipo = -1;
			}
	   	}
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
private:
	double m_lowerBound;
	double m_upperBound;
	int m_proteinSize;
	std::string m_sequence;
	amino *m_aminoAcido; 
	Aleatory m_aleatory;
	void Rotation(double x,double y,double z,double a,double b,double c,double u,double v,double w,double t,double *rx,double *ry,double *rz);
};

double F3dab::evaluate(double individual[], int size)
{
    double w1, w2, LJ, LJ_aux;
    double r, rx, ry, rz, u1,u2,u3,v3;
    double nx,ny,nz,vx,vy,vz,tx,ty,tz,ux,uy,uz; 
    double v1,v2;
    int dim_aux = m_proteinSize - 2;
    unsigned int i = 0;
    unsigned int j = 0;
    amino *amino_pos; 
    
    amino_pos = (amino *) malloc ((m_proteinSize) * sizeof (amino));
	//polar --> cartesian
    amino_pos[0].x = amino_pos[0].y = amino_pos[0].z = 0;
    amino_pos[1].x = amino_pos[1].z = 0;  amino_pos[1].y = 1;

	nx=0;
	ny=0; 
	nz=1;

    for (i = 1; i < m_proteinSize-1; i++)
    {  

		if(individual[i-1]>PI)
		{
			individual[i-1] = m_aleatory.nextDouble(0.0,PI);
		}
		if(individual[i-1]<-PI)
		{
			individual[i-1] = m_aleatory.nextDouble(-PI,0.0);
		}
		if(individual[i+dim_aux-1]>PI)
		{
			individual[i+dim_aux-1] = m_aleatory.nextDouble(0.0,PI);
		}
		if(individual[i+dim_aux-1]<-PI)
		{
			individual[i+dim_aux-1] = m_aleatory.nextDouble(-PI,0.0);
		}	

		tx=0;ty=0;tz=0;
		vx = amino_pos[i].x - amino_pos[i-1].x;
   		vy = amino_pos[i].y - amino_pos[i-1].y;
		vz = amino_pos[i].z - amino_pos[i-1].z;

		Rotation(amino_pos[i].x+vx, amino_pos[i].y+vy, amino_pos[i].z+vz, amino_pos[i].x, amino_pos[i].y, amino_pos[i].z,nx,ny,nz,individual[i-1],&tx,&ty,&tz);

		if (i == 1)
		{
			Rotation(tx,ty,tz,amino_pos[i].x,amino_pos[i].y,amino_pos[i].z,vx,vy,vz,0,&amino_pos[i+1].x,&amino_pos[i+1].y,&amino_pos[i+1].z);		
		}
		else
		{
			Rotation(tx,ty,tz,amino_pos[i].x,amino_pos[i].y,amino_pos[i].z,vx,vy,vz,individual[(i-2)+dim_aux],&amino_pos[i+1].x,&amino_pos[i+1].y,&amino_pos[i+1].z);
		}

		ux = amino_pos[i+1].x - amino_pos[i].x;
	   	uy = amino_pos[i+1].y - amino_pos[i].y;
   		uz = amino_pos[i+1].z - amino_pos[i].z;

  		nx = vy*uz-vz*uy;
  		ny = vz*ux-vx*uz;
  		nz = vx*uy-vy*ux;

    }

	//bond angles
    w1 = 0;
    for (i = 1; i < m_proteinSize-1; i++) 
	{
     	u1 = amino_pos[i].x - amino_pos[i-1].x;
     	u2 = amino_pos[i].y - amino_pos[i-1].y;
     	u3 = amino_pos[i].z - amino_pos[i-1].z;
     	v1 = amino_pos[i+1].x - amino_pos[i].x;
    	v2 = amino_pos[i+1].y - amino_pos[i].y;
     	v3 = amino_pos[i+1].z - amino_pos[i].z;
     	w1 += (u1*v1 + u2*v2 + u3*v3)/(sqrt(pow(u1,2) + pow(u2,2) + pow(u3,2)) * sqrt(pow(v1,2) + pow(v2,2) + pow(v3,2)));
	}


	//torsion angles
    w2 = 0;
    for (i = 1; i < m_proteinSize-2; i++)

    {   
     	u1 = amino_pos[i].x - amino_pos[i-1].x;
    	u2 = amino_pos[i].y - amino_pos[i-1].y;
     	u3 = amino_pos[i].z - amino_pos[i-1].z;
     	v1 = amino_pos[i+2].x - amino_pos[i+1].x;
     	v2 = amino_pos[i+2].y - amino_pos[i+1].y;
     	v3 = amino_pos[i+2].z - amino_pos[i+1].z;
     	w2 = w2 - ((u1*v1 + u2*v2 + u3*v3)/(sqrt(pow(u1,2) + pow(u2,2) + pow(u3,2))*sqrt(pow(v1,2) + pow(v2,2)+pow(v3,2))))/2.; 
    }

	//Lennard-Jones
	LJ = 0;
    for (i = 0; i < m_proteinSize-2; i++)
    {   
		for (j = i + 2; j < m_proteinSize; j++)
        {   
			rx = amino_pos[i].x - amino_pos[j].x;
			ry = amino_pos[i].y - amino_pos[j].y;
			rz = amino_pos[i].z - amino_pos[j].z;
			r = sqrt(rx * rx + ry * ry + rz * rz);

			 LJ_aux = 4 * (1/pow(r, 12) - 1/pow(r, 6));

			if ((m_aminoAcido[i].Tipo != m_aminoAcido[j].Tipo) || (m_aminoAcido[i].Tipo == -1 && m_aminoAcido[j].Tipo == -1))
			{	
				 LJ_aux *= 0.5;
			}
			
			LJ +=  LJ_aux;
        }
    }
    
	free(amino_pos);
	return(w1 + w2 + LJ);
    
}

std::string F3dab::getName()
{
	return "3DAB";
}


double F3dab::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double F3dab::getLowerBound(int index)
{
	return m_lowerBound;
}

double F3dab::getUpperBound(int index)
{
	return m_upperBound;
}

//3D-AB
void F3dab::Rotation(double x,double y,double z,double a,double b,double c,double u,double v,double w,double t,double *rx,double *ry,double *rz)
{
   double u2 = u*u;
   double v2 = v*v;
   double w2 = w*w;
   double cost = cos(t);
   double sint = sin(t);
   double l2 = u2+v2+w2;
   double l = sqrt(l2);

   *rx = (a*(v2+w2)+u*(-b*v-c*w+u*x+v*y+w*z)+(-a*(v2+w2)+u*(b*v+c*w-v*y-w*z)+(v2+w2)*x)*cost+l*(-c*v+b*w-w*y+v*z)*sint)/l2;
   *ry = (b*(u2+w2)+v*(-a*u-c*w+u*x+v*y+w*z)+(-b*(u2+w2)+v*(a*u+c*w-u*x-w*z)+(u2+w2)*y)*cost+l*(c*u-a*w+w*x-u*z)*sint)/l2;
   *rz = (c*(u2+v2)+w*(-a*u-b*v+u*x+v*y+w*z)+(-c*(u2+v2)+w*(a*u+b*v-u*x-v*y)+(u2+v2)*z)*cost+l*(-b*u+a*v-v*x+u*y)*sint)/l2;
}

#endif
