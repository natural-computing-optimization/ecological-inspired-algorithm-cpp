#ifndef F2DAB_H
#define F2DAB_H

#include<math.h>
#include<string>

#include"Protein.h"
#include"../../Aleatory.hpp"


class F2dab : public ObjectiveFunction{
public:
	F2dab(std::string sequence)
	{
		m_lowerBound = -1*PI;
		m_upperBound =  PI;
		m_sequence = sequence;
		std::cout << " Sequence :: " << sequence << std::endl;
		m_proteinSize = m_sequence.size();
		m_aminoAcido = (amino *) malloc ((m_proteinSize) * sizeof (amino));
		for(unsigned int i=0;i < m_proteinSize;i++)
	   	{
			if (m_sequence[i] == 'A' || m_sequence[i] == 'a')
			{
				m_aminoAcido[i].Tipo = 1;
			}
			if (m_sequence[i] == 'B' || m_sequence[i] == 'b')
			{
				m_aminoAcido[i].Tipo = -1;
			}
	   	}
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
private:
	double m_lowerBound;
	double m_upperBound;
	int m_proteinSize;
	std::string m_sequence;
	amino *m_aminoAcido; 
	Aleatory m_aleatory;
};

double F2dab::evaluate(double individual[], int size)
{
    double a_ab,b_ab,c_ab,d_ab;
    double v1,v2;
    amino *amino_pos; 
    unsigned int i = 0;
    unsigned int j = 0;
    
	amino_pos = (amino *) malloc ((m_proteinSize) * sizeof (amino));

    amino_pos[0].x = amino_pos[0].y = 0;
    amino_pos[1].x = 1;
    amino_pos[1].y = 0;
    for (i = 1; (i < (m_proteinSize-1)); i++)
    {   
		if(individual[i]>PI)
		{
			individual[i] = m_aleatory.nextDouble(0.0,PI);
		}
		if(individual[i]<-PI)
		{
			individual[i] = m_aleatory.nextDouble(-PI,0.0);
		}

		a_ab = amino_pos[i].x-amino_pos[i-1].x;
		b_ab = amino_pos[i].y-amino_pos[i-1].y;
		amino_pos[i+1].x = amino_pos[i].x+a_ab*cos(individual[i-1])-b_ab*sin(individual[i-1]);
		amino_pos[i+1].y = amino_pos[i].y+b_ab*cos(individual[i-1])+a_ab*sin(individual[i-1]);
    }

    v1 = 0;
    for (i = 1; (i < (m_proteinSize-1) ); i++) 
    {
		v1 += (1.0-cos(individual[i-1]))/4.0;
	}

    v2 = 0;
    for (i = 0; (i < (m_proteinSize-2)); i++)
    {   
		for (j = (i+2); (j < m_proteinSize); j++)
        {
		   //c_ab = (1.0+Aminoacido[i].Tipo+Aminoacido[j].Tipo+5.0*Aminoacido[i].Tipo*Aminoacido[j].Tipo)/8.0; //Energy for the Bonds AA - BB - AB
			if (m_aminoAcido[i].Tipo == 1 && m_aminoAcido[j].Tipo == 1) //AA bond
			{
				c_ab = 1;
			}
			else if (m_aminoAcido[i].Tipo == -1 && m_aminoAcido[j].Tipo == -1) //BB bond
			{
				c_ab = 0.5;
			}
			else
			{
				c_ab = -0.5; //AB or BA bond
			}

			d_ab = sqrt(((amino_pos[i].x-amino_pos[j].x)*(amino_pos[i].x-amino_pos[j].x))+((amino_pos[i].y-amino_pos[j].y)*(amino_pos[i].y-amino_pos[j].y))); //Distance for Lennard-Jones        
			v2 += 4.0*(1/pow(d_ab,12)-c_ab/pow(d_ab,6));
        }
    }

	free(amino_pos);

	return(v1 + v2);
}

std::string F2dab::getName()
{
	return "2DAB";
}


double F2dab::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double F2dab::getLowerBound(int index)
{
	return m_lowerBound;
}

double F2dab::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
