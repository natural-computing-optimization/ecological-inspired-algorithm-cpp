#ifndef UTILS_H
#define UTILS_H

#include<string>

class Utils{
public:
	std::string toString(int n);
	std::string toString(bool b);
};

std::string Utils::toString(bool n)
{
	if(n)
		return "true";
	return "false";
}

std::string Utils::toString(int n)
{
	int i = 0;               /* Loop counter              */
	int negative = 0;        /* Indicate negative integer */
	int length = 0;          /* Length of string          */
	int temp = 0;            /* Temporary storage         */
	char str[100];

	if((negative = (n<0)))     /* Is it negative?  */
	n = -n;                /* make it positive */

	/* Generate digit characters in reverse order */
	do
	{
	str[i++] = '0'+n%10;    /* Create a rightmost digit        */
	n /= 10;                /* Remove the digit                */              
	}while(n>0);              /* Go again if there's more digits */

	if(negative)              /* If it was negative */
	str[i++] = '-';         /* Append minus       */
	str[i] = '\0';            /* Append terminator  */
	length = i;               /* Save the length    */

	/* Now reverse the string in place */
	/* by switching first and last,    */
	/* second and last but one, etc    */
	for(i = 0 ; i<length/2 ;i++)
	{
	temp = str[i];
	str[i] = str[length-i-1];
	str[length-i-1] = temp;
	}
	return std::string(str);                /* Return the string */
}

#endif
