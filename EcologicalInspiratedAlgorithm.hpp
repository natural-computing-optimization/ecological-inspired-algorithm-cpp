#ifndef ECOLOGICAL_INSPIRATED_ALGORITHM_H
#define ECOLOGICAL_INSPIRATED_ALGORITHM_H

#include <time.h>
#include <stdexcept>

#include "Habitats.hpp"
#include "Configuration.hpp"
#include "ConfigFactory.hpp"
#include "Ecosystem.hpp"
#include "Organism.hpp"
#include "metaheuristics/Metaheuristic.hpp"
#include "metaheuristics/MetaheuristicFactory.hpp"
#include "reports/Status.hpp"
#include "reports/Result.hpp"
#include "reports/ExperimentalResults.hpp"

class EcologicalInspiratedAlgorithm{
public:
	EcologicalInspiratedAlgorithm(std::string fileConfig);
	void execute();
private:
	Configuration m_config;
	Organism globalBest;
	Result run(int runIndex);
	void verifyRestrictions();
	class BackgroundTask
	{
	public:
		BackgroundTask(){
			m_specie = 0;
			m_algorithm = 0;
		};
		BackgroundTask(Specie * specie, Metaheuristic * algorithm)
		{
			m_specie = specie;
			m_algorithm = algorithm;
		};
		void run()
		{
			if(m_algorithm != 0 && m_specie != 0)
				m_algorithm->execute(m_specie);
		};
	private:
		Specie * m_specie;
		Metaheuristic * m_algorithm;
	};
};

EcologicalInspiratedAlgorithm::EcologicalInspiratedAlgorithm(std::string fileConfig)
{
	srand(time(NULL));
	ConfigFactory factory;
	m_config = factory.get(fileConfig);
	m_config.print();
}

void EcologicalInspiratedAlgorithm::verifyRestrictions()
{
	if(m_config.isHabitats() && m_config.getSpecieNumber() < 3)
	{		
		throw std::invalid_argument( "For hierarchical clustering, SPECIE_NUMBER must be greater than 2" );
	}
}

void EcologicalInspiratedAlgorithm::execute()
{

	verifyRestrictions();

	ExperimentalResults exp;
	exp.setEcologicalSucessions(m_config.getEcologicalSucessions());

	initHabitats(m_config.getSpecieNumber());

	for(unsigned short int i = 0; i < m_config.getRuns(); i++)
	{
		Result result = run(i);
		exp.add(result);
	}
	
	destroyHabitats(m_config.getSpecieNumber());

	exp.calcule();
	exp.print();
	
	if(m_config.isConvergence())
	{
		exp.convergenceToFile(m_config.getDirOutput()+"/"+m_config.getObjectiveFunction()->getName()+".convergence");
	}
	if(m_config.isResult())
	{
		exp.resultToFile(m_config.getDirOutput()+"/"+m_config.getObjectiveFunction()->getName()+".result");
	}
	if(m_config.isGlobalBest())
	{
		exp.globalBestToFile(globalBest,m_config.getDirOutput()+"/"+m_config.getObjectiveFunction()->getName()+".globalBest");
	}

}

Result EcologicalInspiratedAlgorithm::run(int runIndex)
{
	unsigned short int s = 0;
	Result result(m_config.getEcologicalSucessions());
	Ecosystem ecosystem(m_config);
	ecosystem.init();
	std::thread threads[m_config.getSpecieNumber()];
	BackgroundTask tasks[m_config.getSpecieNumber()];
	Metaheuristic* algorithms[m_config.getSpecieNumber()];
	MetaheuristicFactory factory(m_config);
	for(s = 0; s < m_config.getSpecieNumber(); s++)
	{
		algorithms[s] = factory.get(m_config.getStrategy(),s);
	}
	time_t t;
	double seconds = (unsigned int) time(&t);
	for(unsigned short int i = m_config.getEcologicalSucessions(); i-- ; )
	{
		for(s = m_config.getSpecieNumber(); s-- ; )
		{
			tasks[s] = BackgroundTask(ecosystem.get(s),algorithms[s]);
			threads[s] = std::thread(&BackgroundTask::run,&tasks[s]);
		}
		for(s = m_config.getSpecieNumber(); s--; )
		{			
			threads[s].join();
		}
		if(m_config.isHabitats())
		{
			ecosystem.phaseHabitat(runIndex, i);
		}
		result.add(ecosystem.getStatus());
	}
	result.setBestGlobalObjectiveFunction(ecosystem.getGlobalBest().getObjectiveFunction());
	result.setEvaluations(ecosystem.getAvgEvaluations());
	seconds = time(&t) - seconds;
	result.setTime(seconds);
	if(runIndex == 0 || result.getBestGlobalObjectiveFunction() < globalBest.getObjectiveFunction())
	{
		globalBest = ecosystem.getGlobalBest();
	}
	if(m_config.isHabitats() && m_config.isNumberHabitats())
	{
		ecosystem.printHabitatSize(runIndex);
	}
	std::cout << "| Run : " << runIndex << " Best Global : " << result.getBestGlobalObjectiveFunction() << " Time secs : " << seconds << std::endl;
	return result;
}

#endif
