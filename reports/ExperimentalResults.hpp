#ifndef EXPERIMENTAL_RESULTS_H
#define EXPERIMENTAL_RESULTS_H


#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#include"Result.hpp"
#include"../Organism.hpp"

using namespace std;

class ExperimentalResults{
public:
	void add(Result result);
	void setEcologicalSucessions(int ecologicalSucessions);
	void print();
	void calcule();
	void convergenceToFile(string fileName);
	void resultToFile(string fileName);
	void globalBestToFile(Organism globalBest, string fileName);
private:
	vector<Result> m_list;
	double m_avgObjectiveFunction;
	double m_stdObjectiveFunction;
	double m_avgEvaluations;
	double m_stdEvaluations;
	double m_avgTime;
	double m_stdTime;
	void calculeAvgObjectiveFunction();
	void buildConvergence();
	void buildGenotypic();
	void buildPhenotypic();
	void calculeAvgEvaluations();
	void calculeAvgTime();
	vector<double> m_convergence;
	vector<double> m_genotypic;
	vector<double> m_phenotypic;
	int m_ecologicalSucessions;
};

void ExperimentalResults::setEcologicalSucessions(int ecologicalSucessions)
{
	m_ecologicalSucessions = ecologicalSucessions;
	m_list.reserve(m_ecologicalSucessions);
}

void ExperimentalResults::add(Result result)
{
	m_list.push_back(result);
}

void ExperimentalResults::print()
{
	cout << "|=========================================|" << endl;
	cout << "|          EXPERIMENTAL RESULTS           |" << endl;
	cout << "|=========================================|" << endl;		
	cout << "| Avg Objetive Function : " << m_avgObjectiveFunction << " std: " << m_stdObjectiveFunction << endl;
	cout << "| Avg Evaluations       : " << m_avgEvaluations << " std: " << m_stdEvaluations << endl;
	cout << "| Avg Time secs         : " << m_avgTime << " std: " << m_stdTime << endl;
	cout << "|=========================================|" << endl;		
}

void ExperimentalResults::calculeAvgObjectiveFunction()
{
	unsigned short int a;
	m_avgObjectiveFunction = 0;
	for(a=0; a < m_list.size(); a++)
	{
		m_avgObjectiveFunction += m_list[a].getBestGlobalObjectiveFunction();
	}
	m_avgObjectiveFunction /= m_list.size();
	m_stdObjectiveFunction = 0;
	for(a=0; a < m_list.size(); a++)
	{
		m_stdObjectiveFunction += pow((m_avgObjectiveFunction - m_list[a].getBestGlobalObjectiveFunction()),2);
	}
	m_stdObjectiveFunction /= m_list.size();
	m_stdObjectiveFunction = sqrt(m_stdObjectiveFunction);
}

void ExperimentalResults::calcule()
{
	calculeAvgObjectiveFunction();
	calculeAvgEvaluations();
	calculeAvgTime();
	buildConvergence();
	buildPhenotypic();
	buildGenotypic();
}

void ExperimentalResults::calculeAvgEvaluations()
{
	unsigned short int a;
	m_avgEvaluations = 0;
	for(a=0; a < m_list.size(); a++)
	{
		m_avgEvaluations += m_list[a].getEvaluations();
	}
	m_avgEvaluations /= m_list.size();
	m_stdEvaluations = 0;
	for(a=0; a < m_list.size(); a++)
	{
		m_stdEvaluations += pow((m_avgEvaluations - m_list[a].getEvaluations()),2);
	}
	m_stdEvaluations /= m_list.size();
	m_stdEvaluations = sqrt(m_stdEvaluations);
}

void ExperimentalResults::calculeAvgTime()
{
	
	unsigned short int a;
	m_avgTime = 0;
	for(a=0; a < m_list.size(); a++)
	{
		m_avgTime += m_list[a].getTime();
	}
	m_avgTime /= m_list.size();
	m_stdTime = 0;
	for(a=0; a < m_list.size(); a++)
	{
		m_stdTime += pow((m_avgTime - m_list[a].getTime()),2);
	}
	m_stdTime /= m_list.size();
	m_stdTime = sqrt(m_stdTime);
}

void ExperimentalResults::buildConvergence()
{
	unsigned short int a;
	unsigned short int b;
	m_convergence.reserve(m_ecologicalSucessions);
	for(a = 0; a < m_ecologicalSucessions; a++)
	{
		m_convergence.push_back(0);
	}
	for(a=0; a < m_list.size(); a++)
	{
		for( b = 0; b < m_ecologicalSucessions; b++)
		{
			m_convergence[b] += m_list[a].getStatus(b).getObjectiveFunction();
		}
	}
	for( b = 0; b < m_ecologicalSucessions; b++)
	{
		m_convergence[b] /= m_list.size();
	}
}

void ExperimentalResults::buildGenotypic()
{
	unsigned short int a;
	unsigned short int b;
	m_genotypic.reserve(m_ecologicalSucessions);
	for(a = 0; a < m_ecologicalSucessions; a++)
	{
		m_genotypic.push_back(0);
	}
	for(a=0; a < m_list.size(); a++)
	{
		for( b = 0; b < m_ecologicalSucessions; b++)
		{
			m_genotypic[b] += m_list[a].getStatus(b).getGenotypicDiversity();
		}
	}
	for(b = 0; b < m_ecologicalSucessions; b++)
	{
		m_genotypic[b] /= m_list.size();
	}
}

void ExperimentalResults::buildPhenotypic()
{
	unsigned short int a;
	unsigned short int b;
	m_phenotypic.reserve(m_ecologicalSucessions);
	for(a = 0; a < m_ecologicalSucessions; a++)
	{
		m_phenotypic.push_back(0);
	}
	for(a = 0; a < m_list.size(); a++)
	{
		for(b = 0; b < m_ecologicalSucessions; b++)
		{
			m_phenotypic[b] += m_list[a].getStatus(b).getPhenotypicDiversity();
		}
	}
	for(b = 0; b < m_ecologicalSucessions; b++)
	{
		m_phenotypic[b] /= m_list.size();
	}
}

void ExperimentalResults::convergenceToFile(string fileName)
{
	ofstream output(fileName.c_str());
	output << "#========================================================#" << endl;
	output << "# ECO_STEP # Obj Function # Genotypic # Phenotypic       #" << endl;
	output << "#========================================================#" << endl;
	for(unsigned short int b = 0; b < m_ecologicalSucessions; b++)
	{
		output << b << " " << m_convergence[b] << " " << m_genotypic[b] << " " << m_phenotypic[b] << endl;
	}
}


void ExperimentalResults::resultToFile(string fileName)
{
	ofstream output(fileName.c_str());
	output << "#========================================================#" << endl;
	output << "# Result avg +/- std                                     #" << endl;
	output << "# Evaluations avg +/- std                                #" << endl;
	output << "# Time secs avg +/- std                                  #" << endl;
	output << "#========================================================#" << endl;
	output << m_avgObjectiveFunction << " +/- " << m_stdObjectiveFunction << endl;
	output << m_avgEvaluations << " +/- " << m_stdEvaluations << endl;
	output << m_avgTime << " +/- " << m_stdTime << endl;
	output << "#========================================================#" << endl;
	output << "# Run # objective function # evaluations # time          #" << endl;
	output << "#========================================================#" << endl;
	for(unsigned short int a=0; a < m_list.size(); a++)
	{	
		output << a << " " << m_list[a].getBestGlobalObjectiveFunction() << " " << m_list[a].getEvaluations() << " " << m_list[a].getTime() <<  endl;
	}
}

void ExperimentalResults::globalBestToFile(Organism globalBest, string fileName)
{
	ofstream output(fileName.c_str());
	output << "#========================================================#" << endl;
	output << "# GLOBAL BEST                                            #" << endl;
	output << "#========================================================#" << endl;
	output << " Objective Function : " << globalBest.getObjectiveFunction() << endl;
	output << " Fitness Function : " << globalBest.getFitness() << endl;
	output << "#========================================================#" << endl;
	output << globalBest.size() << " dimensions " << endl;
	output << "#========================================================#" << endl;
	for(unsigned short int a=0; a < globalBest.size(); a++)
	{
		output << globalBest[a] << " " ;
	}
}


#endif
