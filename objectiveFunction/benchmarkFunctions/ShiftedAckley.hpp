#ifndef SHIFTED_ACKLEY_H
#define SHIFTED_ACKLEY_H

#include <math.h>
#include <string>
#include "ShiftedAckleyData.h"


class ShiftedAckley : public ObjectiveFunction{
public:
	ShiftedAckley()
	{
		m_lowerBound = -32;
		m_upperBound =  32;
		m_fBias = -140.0;
		PI = acos(-1.0);
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -140.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
	double PI;
};

double ShiftedAckley::evaluate(double individual[], int size)
{
	double z = 0;
	double Fx = 0;
	double top1 = 0;
	double top2 = 0;
	unsigned short int i;

	Fx = 0;
	for(i=0;i<size;i++){   
		z = individual[i] - ackley[i];
		top1 = top1 + pow(z , 2 );
		top2 = top2 + cos(2*PI*z);
	}
	Fx = -20*exp(-0.2*sqrt(top1/size)) -exp(top2/size) + 20 + exp(1.0) + m_fBias;
	
	if(Fx - getOptimalPoint(size) <= 10e-20)
	{
		Fx = getOptimalPoint(size);
	}
	return Fx;
}

std::string ShiftedAckley::getName()
{
	return "ShiftedAckley";
}


double ShiftedAckley::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedAckley::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedAckley::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
