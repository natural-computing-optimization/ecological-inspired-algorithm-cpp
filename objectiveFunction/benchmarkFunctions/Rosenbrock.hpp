#ifndef ROSENBROCK_H
#define ROSENBROCK_H

#include<math.h>
#include<string>


class Rosenbrock : public ObjectiveFunction{
public:
	Rosenbrock()
	{
		m_lowerBound = -30;
		m_upperBound =  30;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Rosenbrock::evaluate(double individual[], int size)
{
	double top = 0;
	unsigned short int i;
	for (i = 0; i < size -1 ; i++)
        {
        	top=top+100.*pow((individual[i+1] - pow(individual[i],2.)),2) + pow((1. - individual[i]),2);
        }
	if(top - getOptimalPoint(size) <= 10e-20)
	{
		top = getOptimalPoint(size);
	}
	return top;
}

std::string Rosenbrock::getName()
{
	return "Rosenbrock";
}


double Rosenbrock::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Rosenbrock::getLowerBound(int index)
{
	return m_lowerBound;
}

double Rosenbrock::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
