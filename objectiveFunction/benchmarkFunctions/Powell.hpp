#ifndef POWELL_H
#define POWELL_H

#include<math.h>
#include<string>


class Powell : public ObjectiveFunction{
public:
	Powell()
	{
		m_lowerBound = -4;
		m_upperBound =  5;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Powell::evaluate(double individual[], int size)
{
	double aux = 0;
	unsigned short int j;
	for (j = 1; j <= (int)size/4; j++) {
		aux +=  pow(individual[4*j-4] + 10 * individual[4*j-3],2.0)
        	  + 5 * pow(individual[4*j-2] - individual[4*j-1],2.0)
        	  + pow(individual[4*j-3] - 2 * individual[4*j-2], 4.0)
		  + 10 * pow(individual[4*j - 4] - individual[4*j-1], 4.0);
	}
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Powell::getName()
{
	return "Powell";
}


double Powell::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Powell::getLowerBound(int index)
{
	return m_lowerBound;
}

double Powell::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
