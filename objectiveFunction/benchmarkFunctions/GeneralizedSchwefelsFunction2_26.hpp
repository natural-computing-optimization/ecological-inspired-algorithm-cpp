#ifndef GENERALIZED_SCHWEFELS_FUNCTION_2_26_H
#define GENERALIZED_SCHWEFELS_FUNCTION_2_26_H

#include<math.h>
#include<string>


class GeneralizedSchwefelsFunction2_26 : public ObjectiveFunction{
public:
	GeneralizedSchwefelsFunction2_26()
	{
		m_lowerBound = -500;
		m_upperBound =  500;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -418.982887272433;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double GeneralizedSchwefelsFunction2_26::evaluate(double individual[], int size)
{
	double aux = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	aux += individual[i]*sin(sqrt(fabs(individual[i]))); 
        }
	aux =  (-1.0 * aux/ size);
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string GeneralizedSchwefelsFunction2_26::getName()
{
	return "GeneralizedSchwefelsFunction2_26";
}


double GeneralizedSchwefelsFunction2_26::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double GeneralizedSchwefelsFunction2_26::getLowerBound(int index)
{
	return m_lowerBound;
}

double GeneralizedSchwefelsFunction2_26::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
