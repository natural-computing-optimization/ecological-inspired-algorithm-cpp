#ifndef RANA_H
#define RANA_H

#include<math.h>
#include<string>


class Rana : public ObjectiveFunction{
public:
	Rana()
	{
		m_lowerBound = -512;
		m_upperBound =  512;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -511.70430 * size + 511.68714;
	};
	
private:
	double m_lowerBound;
	double m_upperBound;
};

double Rana::evaluate(double individual[], int size)
{
	double t1 = 0;
	double t2 = 0;
	double sum = 0;
	unsigned short int i;
	sum = 0.0;
	for (i = 0; i < size-1; i++) {
		t1 = sqrt(fabsf(individual[i+1] + individual[i] + 1.0));
		t2 = sqrt(fabsf(individual[i+1] - individual[i] + 1.0));
		sum += (individual[i+1] + 1.0) * cos(t2) * sin(t1) + cos(t1) * sin(t2) * individual[i];
	}
	sum =  sum/(double)(size-1);
	
	if(sum - getOptimalPoint(size) <= 10e-20)
	{
		sum = getOptimalPoint(size);
	}
	return sum;
}

std::string Rana::getName()
{
	return "Rana";
}


double Rana::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Rana::getLowerBound(int index)
{
	return m_lowerBound;
}

double Rana::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
