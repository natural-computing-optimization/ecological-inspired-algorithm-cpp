#ifndef GENERALIZED_HOLZMAN_H
#define GENERALIZED_HOLZMAN_H

#include<math.h>
#include<string>


class GeneralizedHolzman : public ObjectiveFunction{
public:
	GeneralizedHolzman()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double GeneralizedHolzman::evaluate(double individual[], int size)
{
	double aux = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	aux += i * pow(individual[i] , 4);
        }
    if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string GeneralizedHolzman::getName()
{
	return "GeneralizedHolzman";
}


double GeneralizedHolzman::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double GeneralizedHolzman::getLowerBound(int index)
{
	return m_lowerBound;
}

double GeneralizedHolzman::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
