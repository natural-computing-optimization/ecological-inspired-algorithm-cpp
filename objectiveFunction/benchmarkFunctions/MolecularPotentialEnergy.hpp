#ifndef MOLECULAR_POTENTIAL_ENERGY_H
#define MOLECULAR_POTENTIAL_ENERGY_H

#include<math.h>
#include<string>


class MolecularPotentialEnergy : public ObjectiveFunction{
public:
	MolecularPotentialEnergy()
	{
		m_lowerBound = -0;
		m_upperBound =  5;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -0.0411183034*size;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double MolecularPotentialEnergy::evaluate(double individual[], int size)
{
	double aux1 = 0;
	double aux2 = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
	{
		aux1 += 1 + cos(3*individual[i]);
		aux2 += (pow(-1,i) / (sqrt(fabs(10.60099896-4.141720682 * cos(individual[i])))));
	}
	aux1 = aux1 + aux2;
	if(aux1 - getOptimalPoint(size) <= 10e-20)
	{
		aux1 = getOptimalPoint(size);
	}
	return aux1;
}

std::string MolecularPotentialEnergy::getName()
{
	return "MolecularPotentialEnergy";
}


double MolecularPotentialEnergy::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double MolecularPotentialEnergy::getLowerBound(int index)
{
	return m_lowerBound;
}

double MolecularPotentialEnergy::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
