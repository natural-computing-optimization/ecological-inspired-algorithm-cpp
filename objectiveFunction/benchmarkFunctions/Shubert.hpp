#ifndef SHUBERT_H
#define SHUBERT_H

#include<math.h>
#include<string>


class Shubert : public ObjectiveFunction{
public:
	Shubert()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -24.06;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Shubert::evaluate(double individual[], int size)
{
	double sum = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	sum += -sin(2.0*individual[i]+1.0)
	          -2.0*sin(3.0*individual[i]+2.0)
        	  -3.0*sin(4.0*individual[i]+3.0)
        	  -4.0*sin(5.0*individual[i]+4.0)
        	  -5.0*sin(6.0*individual[i]+5.0);
        }
	sum =  sum/(size/2.0);	
	if(sum - getOptimalPoint(size) <= 10e-20)
	{
		sum = getOptimalPoint(size);
	}
	return sum;
}

std::string Shubert::getName()
{
	return "Shubert";
}


double Shubert::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Shubert::getLowerBound(int index)
{
	return m_lowerBound;
}

double Shubert::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
