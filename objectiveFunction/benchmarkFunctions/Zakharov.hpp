#ifndef ZAKHAROV_H
#define ZAKHAROV_H

#include<math.h>
#include<string>


class Zakharov : public ObjectiveFunction{
public:
	Zakharov()
	{
		m_lowerBound = -5;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Zakharov::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	aux = aux + pow(individual[i],2.0);
	   	aux1 = aux1+0.5*i*individual[i];
        }
	aux =  ( aux+pow(aux1,2.0)+pow(aux1,4.0) );
	
    if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

std::string Zakharov::getName()
{
	return "Zakharov";
}


double Zakharov::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Zakharov::getLowerBound(int index)
{
	return m_lowerBound;
}

double Zakharov::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
