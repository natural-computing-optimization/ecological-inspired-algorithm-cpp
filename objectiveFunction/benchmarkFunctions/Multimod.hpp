#ifndef MULTIMOD_H
#define MULTIMOD_H

#include<math.h>
#include<string>


class Multimod : public ObjectiveFunction{
public:
	Multimod()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	};
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Multimod::evaluate(double individual[], int size)
{
	double s = 0;
	double p = 0;
	double t = 0;
	unsigned short int i;
	p = fabs(individual[0]);
	s = p;
	for (i = 1; i < size; i++)
        {        	
     		t = fabs(individual[i]);
     		s += t;
     		p *= t;
        }
	s =  (s+p);
	if(s - getOptimalPoint(size) <= 10e-20)
	{
		s = getOptimalPoint(size);
	}
	return s;
}

std::string Multimod::getName()
{
	return "Multimod";
}


double Multimod::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Multimod::getLowerBound(int index)
{
	return m_lowerBound;
}

double Multimod::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
