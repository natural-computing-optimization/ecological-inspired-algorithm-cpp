#ifndef LEVY_FUNCTION_H
#define LEVY_FUNCTION_H

#include<math.h>
#include<string>


class LevyFunction : public ObjectiveFunction{
public:
	LevyFunction()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
		PI = 3.14159265;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double PI;
};

double LevyFunction::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	double y[size];
	for (i = 0; i< size; i++)
	{
		y[i] = 1+(individual[i]-1)/4.0;
	}
	aux = pow(sin(PI*y[0]),2.0);
	for (i = 0; i<size-1;i++)
	    aux = aux + pow(y[i]-1,2.0)*(1+10*pow(sin(PI*y[i]+1),2.0));

	aux = aux+pow(y[size-1]-1,2.0)*(1+pow(sin(2*PI*y[size-1]),2.0) );
	
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return ( aux );
}

std::string LevyFunction::getName()
{
	return "LevyFunction";
}


double LevyFunction::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double LevyFunction::getLowerBound(int index)
{
	return m_lowerBound;
}

double LevyFunction::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
