#ifndef GENERALIZED_PENALIZED_FUNCTION_2_H
#define GENERALIZED_PENALIZED_FUNCTION_2_H

#include<math.h>
#include<string>


class GeneralizedPenalizedFunction2 : public ObjectiveFunction{
public:
	GeneralizedPenalizedFunction2()
	{
		m_lowerBound = -50;
		m_upperBound =  50;
		PI = 3.14159265;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 1.34e-32;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double PI;
	double TempValue(double x,int a,int k,int m);
};

double GeneralizedPenalizedFunction2::evaluate(double individual[], int size)
{
	double aux = 0;
	double aux1 = 0;
	unsigned short int i;
	
	for (i=0;i<size-1;i++)
	{
		aux += pow(individual[i]-1,2.0)*(1.0+10.0*pow(sin(3*PI*individual[i+1]),2.0)); 
	}
	for (i=0;i<size;i++)
	{
		aux1 += TempValue(individual[i],5,100,4);
	}

	aux =  ( (pow(sin(3.0*PI*individual[0]),2.0)+aux+pow(individual[size-1]-1,2.0)
		*(1.0+pow(sin(2.0*PI*individual[size-1]),2.0)))/10.0+aux1 );
	if(aux - getOptimalPoint(size) <= 10e-20)
	{
		aux = getOptimalPoint(size);
	}
	return aux;
}

double GeneralizedPenalizedFunction2::TempValue(double x,int a,int k,int m)
{
	double temp = 0.0;
	if( x > a)
	{
		temp = k*pow(x-a,m);
	}
	else if( x <= a && x >= -a)
	{
		temp = 0.0;
	}
	else
	{
		temp = k*pow(-x-a,m);
	}
	return temp;
}

std::string GeneralizedPenalizedFunction2::getName()
{
	return "GeneralizedPenalizedFunction2";
}


double GeneralizedPenalizedFunction2::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double GeneralizedPenalizedFunction2::getLowerBound(int index)
{
	return m_lowerBound;
}

double GeneralizedPenalizedFunction2::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
