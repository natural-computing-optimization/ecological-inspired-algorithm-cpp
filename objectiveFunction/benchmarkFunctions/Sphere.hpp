#ifndef SPHERE_H
#define SPHERE_H

#include<math.h>
#include<string>


class Sphere : public ObjectiveFunction{
public:
	Sphere()
	{
		m_lowerBound = -100;
		m_upperBound =  100;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Sphere::evaluate(double individual[], int size)
{
	double top = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	top=top+individual[i]*individual[i];
        }
    if(top - getOptimalPoint(size) <= 10e-20)
	{
		top = getOptimalPoint(size);
	}
	return top;
}

std::string Sphere::getName()
{
	return "Sphere";
}


double Sphere::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Sphere::getLowerBound(int index)
{
	return m_lowerBound;
}

double Sphere::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
