#ifndef SHIFTED_GRIEWANK_H
#define SHIFTED_GRIEWANK_H

#include <math.h>
#include <string>
#include "ShiftedGriewankData.h"


class ShiftedGriewank : public ObjectiveFunction{
public:
	ShiftedGriewank()
	{
		m_lowerBound = -600;
		m_upperBound =  600;
		m_fBias = -180.0;
		PI = acos(-1.0);
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return -180.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
	double m_fBias;
	double PI;
};

double ShiftedGriewank::evaluate(double individual[], int size)
{
	double z = 0;
	double top1 = 0;
	double top2 = 0;
	unsigned short int i;

	top1 = 0;
	top2 = 1;
	for(i=0;i<size;i++){       
		z = individual[i] - griewank[i];
		top1 = top1 + ( pow(z,2) / 4000 );
		top2 = top2 * ( cos(z/sqrt(i+1)));
	}
	top1 =  (top1 - top2 + 1 + m_fBias);
	if(top1 - getOptimalPoint(size) <= 10e-20)
	{
		top1 = getOptimalPoint(size);
	}
	return top1;
}

std::string ShiftedGriewank::getName()
{
	return "ShiftedGriewank";
}


double ShiftedGriewank::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedGriewank::getLowerBound(int index)
{
	return m_lowerBound;
}

double ShiftedGriewank::getUpperBound(int index)
{
	return m_upperBound;
}


#endif
