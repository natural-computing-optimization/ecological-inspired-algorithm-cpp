#ifndef STRETCHEDV_H
#define STRETCHEDV_H

#include<math.h>
#include<string>


class StretchedV : public ObjectiveFunction{
public:
	StretchedV()
	{
		m_lowerBound = -10;
		m_upperBound =  10;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double StretchedV::evaluate(double individual[], int size)
{
	double aux = 0;
	double sum = 0;
	unsigned short int i;
	for (i = 0; i < size -1; i++)
        {
        	aux = individual[i+1]*individual[i+1] + individual[i]*individual[i];
	    	sum += pow(aux, 0.25) * (pow(sin(50.0 * pow(aux, 0.1)), 2.0)+1.0);
        }
    if(sum - getOptimalPoint(size) <= 10e-20)
	{
		sum = getOptimalPoint(size);
	}
	return sum;
}

std::string StretchedV::getName()
{
	return "StretchedV";
}


double StretchedV::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double StretchedV::getLowerBound(int index)
{
	return m_lowerBound;
}

double StretchedV::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
