#ifndef GRIEWANK_H
#define GRIEWANK_H

#include<math.h>
#include<string>


class Griewank : public ObjectiveFunction{
public:
	Griewank()
	{
		m_lowerBound = -600;
		m_upperBound =  600;
	}
	virtual double evaluate(double individual[], int size);
	virtual std::string getName();
	virtual double verifyBounds(int index, double d);
	virtual double getLowerBound(int index);
	virtual double getUpperBound(int index);
	virtual double getOptimalPoint(int size)
	{
		return 0.0;
	};
private:
	double m_lowerBound;
	double m_upperBound;
};

double Griewank::evaluate(double individual[], int size)
{
	double top1 = 0;
	double top2 = 1;
	for(unsigned short int j = 0 ; j < size; j++)
        {
		top1=top1+pow((individual[j]),(double)2);
        	top2=top2*cos((((individual[j])/sqrt((double)(j+1)))*M_PI)/180);
        }
	top1 =  (1.0/(double)4000.0)*top1-top2+1.0;
	if(top1 - getOptimalPoint(size) <= 10e-20)
	{
		top1 = getOptimalPoint(size);
	}
	return top1;
}

std::string Griewank::getName()
{
	return "Griewank";
}


double Griewank::verifyBounds(int index, double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double Griewank::getLowerBound(int index)
{
	return m_lowerBound;
}

double Griewank::getUpperBound(int index)
{
	return m_upperBound;
}

#endif
