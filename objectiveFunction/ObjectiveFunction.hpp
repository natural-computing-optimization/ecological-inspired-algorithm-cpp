#ifndef OBJECTIVE_FUNCTION_H
#define OBJECTIVE_FUNCTION_H

#include<string>

class ObjectiveFunction{
public:
	virtual double evaluate(double individual[], int size) = 0;
	virtual std::string getName() = 0;
	virtual double verifyBounds(int index, double d) = 0;
	virtual double getLowerBound(int index) = 0;
	virtual double getUpperBound(int index) = 0;
	virtual double getOptimalPoint(int size) = 0;
};

#endif
