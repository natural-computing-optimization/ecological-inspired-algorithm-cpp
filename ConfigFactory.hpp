#ifndef CONFIG_FACTORY_H
#define CONFIG_FACTORY_H

#include <string>
#include <fstream>

#include "Configuration.hpp"
#include "fitnessFunction/FitnessFactory.hpp"
#include "objectiveFunction/ObjectiveFunctionFactory.hpp"
#include "diversity/genotypic/GenotypicFactory.hpp"
#include "diversity/phenotypic/PhenotypicFactory.hpp"

class ConfigFactory{
public:
	Configuration get(std::string fileName);
};

Configuration ConfigFactory::get(std::string fileName)
{
	Configuration config;
	std::ifstream file(fileName);
	std::string   line;
	std::string  key;
	std::string  value;
	std::string  temp;
	size_t sepPos;
	while(std::getline(file, line))
	{
    		temp = line;
		temp.erase(0, temp.find_first_not_of("\t "));
		sepPos = temp.find('=');
		key = temp.substr(0, sepPos);
		if (key.find('\t') != line.npos || key.find(' ') != temp.npos)
			key.erase(key.find_first_of("\t "));
		value = temp.substr(sepPos + 1);
		value.erase(0, value.find_first_not_of("\t "));
		value.erase(value.find_last_not_of("\t ") + 1);
		if(key == "RUNS")
		{
			config.setRuns(std::stoi(value));
		}
		else if(key == "ECOLOGICAL_SUCESSIONS")
		{
			config.setEcologicalSucessions(std::stoi(value));
		}
		else if(key == "EVOLUTIONARY_PERIOD")
		{
			config.setEvolutionaryPeriod(std::stoi(value));
		}
		else if(key == "SPECIE_NUMBER")
		{
			config.setSpecieNumber(std::stoi(value));
		}
		else if(key == "SPECIE_SIZE")
		{
			config.setSpecieSize(std::stoi(value));
		}
		else if(key == "DIMENSIONS")
		{
			config.setDimensions(std::stoi(value));
		}
		else if(key == "OBJECTIVE_FUNCTION")
		{
			ObjectiveFunctionFactory factory;
			config.setObjectiveFunction(factory.get(value));			
		}
		else if(key == "FITNESS_FUNCTION")
		{
			FitnessFactory factory;
			config.setFitnessFunction(factory.get(value));
		}
		else if(key == "STRATEGY")
		{
			config.setStrategy(value);
		}
		else if(key == "DIRECTORY_OUTPUT")
		{
			config.setDirOutput(value);
		}
		else if(key == "REPORT_CONVERGENCE")  
		{
			config.setConvergence(value == "TRUE");
		}
		else if(key == "REPORT_RESULTS") 
		{
			config.setResult(value == "TRUE");
		}
		else if(key == "GENOTYPIC_DIVERSITY")
		{
			GenotypicFactory factory;
			config.setGenotypic(factory.get(value));
		}
		else if(key == "PHENOTYPIC_DIVERSITY")
		{
			PhenotypicFactory factory;
			config.setPhenotypic(factory.get(value));
		}
		else if(key == "HABITATS") 
		{
			config.setHabitats(value == "TRUE");
		}
		else if(key == "REPORT_DENDOGRAM") 
		{
			config.setSingleLinkClustering(value == "TRUE");
		}
		else if(key == "REPORT_NUMBER_OF_HABITATS") 
		{
			config.setNumberHabitats(value == "TRUE");
		}
		else if(key == "SYMBIOTIC_RELATIONSHIP") 
		{
			config.setSymbioticRelationship(value == "TRUE");
		}
		else if(key == "TOURNAMENT_SIZE")
		{
			config.setTournamentSize(std::stoi(value));
		}
		else if(key == "REPORT_GLOBAL_BEST") 
		{
			config.setGlobalBest(value == "TRUE");
		}
		else if(key == "TYPE_SYMBIOTIC_RELATIONSHIP") 
		{
			config.setTypeSymbioticRelationship(value);
		}
	}
	return config;
}

#endif
