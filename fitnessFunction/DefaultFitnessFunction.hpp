#ifndef DEFAULT_FITNESS_FUNCTION_H
#define DEFAULT_FITNESS_FUNCTION_H

#include <math.h>

#include <string>

class DefaultFitnessFunction : public FitnessFunction{
public:
	virtual double evaluate(double objectiveFunction);
	virtual std::string getName();
};

double DefaultFitnessFunction::evaluate(double objectiveFunction)
{
	if(objectiveFunction >= 0.0)
 	{
		 return 1.0 / (objectiveFunction + 1.0);
 	}
 	return 1 + fabs(objectiveFunction);
}

std::string DefaultFitnessFunction::getName()
{
	return "Default Fitness Function";
}

#endif
