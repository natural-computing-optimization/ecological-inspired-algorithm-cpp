#ifndef GENOTYPIC_FACTORY_H
#define GENOTYPIC_FACTORY_H

#include <stdexcept>
#include <string>

#include "GenotypicDiversityMeasure.hpp"
#include "DefaultGenotypicDiversityMeasure.hpp"

class GenotypicFactory{
public:
	GenotypicDiversityMeasure * get(std::string name);
};

GenotypicDiversityMeasure * GenotypicFactory::get(std::string name)
{
	if(name == "DEFAULT")
	{
		return new DefaultGenotypicDiversityMeasure();
	}
	throw std::invalid_argument( "received invalid Fitness Function name" );
}

#endif
